
# Socket.IO Chat UI

Przykładowa aplikacja chat (UI) oparta o websockety.

## Uruchomienie

```
$ npm install
$ npm start
```

Domyślnie aplikacja nasłuchuje na porcie 8080